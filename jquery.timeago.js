/* jQuery.Timeago
/* Author: Yves Lange
/* This plugin will translate the content/text of elements
/* into "ago" time like: "X sec ago"
*/
(function($) {
  $.fn.timeago=function(inter, prefix, suffix) {
    prefix = prefix || "";
    suffix = suffix || "ago";
    var sel = this.selector;
    var elv = "span.timeago_v";
    var elt = "span.timeago_t";

    // Init
    this.each(function(){
      var timestamp = $(this).text();
      $(this).text("");
      $(this).append("<span class='timeago_v'>"+timestamp+"</span>");
      $(this).append("<span class='timeago_t'></span>");
      $(this).find(".timeago_v").hide();
    });

    // Update
    var timeagoINTERVAL = setInterval(function(){
      var cur = Math.round((new Date()).getTime() / 1000);

      $(sel).each(function(){
        var t = parseInt($(this).find(elv).text());
        var ago = new Date((cur-t)*1000);
        var text = ago.getSeconds();
        var unit = "sec";
        if(ago.getHours() > 0){ text = ago.getHours(); unit = "h"; }
        else if(ago.getMinutes() > 0){ text = ago.getMinutes(); unit = "min";}
        $(this).find(elt).text(prefix+" "+(text)+""+unit+" "+suffix);
      });
    },inter);
  };

  return this;
})(jQuery);

$("div.timeago").timeago(900, "Passed", "ago");
