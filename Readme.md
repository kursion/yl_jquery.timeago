# jQuery.Timeago

This is a plugin for jquery which allow you to parse
timestamp into "ago" time readable by humans (eg: 1380630213
is translated into "3 min ago")

# How to
Import the plugin after jquery
```html
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js" charset="utf-8"></script>
  <script type="text/javascript" src="jquery.timeago.js" charset="utf-8"></script>
```

Then create your elements containing a single timestamp as text
```html
  <div class="timeago">1380630213</div>
```

Finally, call the plugin to initiate and update the timers.
```javascript
  $("div.timeago").timeago(900, "Passed", "ago");
```

# Parameters
1. **inter**: is the interval between two refreshes
2. **prefix** (optionnal): is the prefix you want to add (eg: passed)
3. **suffix** (optionnal): is the suffix you want to add (eg: ago)
